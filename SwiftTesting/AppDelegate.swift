import UIKit

class Dog {
	var breed:String = "Beagle"
}

struct DogStruct {
	var breed:String = "Beagle"

}

class ClosureOwner:NSObject {
	var closure:((Void)->Void)? = nil
	
	deinit {
		NSLog("** deinit ClosureOwner")
		
	}
}

class MyObject : NSObject {
	
	var closure:((Void)->Void)? = nil
	var closureOwner = ClosureOwner()
	
	// Lazy test
	
	var notLazy:String = {
		let string = "Not being lazy test"
		print(string)
		return string
	}()
	
	lazy var howLazy:String = {
		let string = "lazy test"
		print(string)
		return string
	}()
	
	static var sharedManager: String = {
		let string = "How lazy am I?"
		print(string)
		return string
	}()
	
	// Get Test ... do not get confused
	var getTest1:NSTimeInterval {
		return NSDate().timeIntervalSinceReferenceDate
	}
	
	var getTest2:NSTimeInterval = {
		return NSDate().timeIntervalSinceReferenceDate
	}()
	
	deinit {
		closure = nil
		NSLog("** deinit MyObject")
	}
	
	func showAlert(fromViewController controller:UIViewController) {
		let alert = UIAlertController(title: "Alert", message: "Alert Message", preferredStyle: .Alert)
		
		let action1 = UIAlertAction(title: "Item1", style: .Default) { //[weak/unowned self /* this is not necessary because self is not captured */]
			(action) -> Void in
			self.action1("alert action")
			let _ = self.howLazy
		}
		alert.addAction(action1)
		controller.presentViewController(alert, animated: true, completion: {
			//[weak/unowned self /* this is not necessary because self is not captured */]
			self.action1("alert presented")
		})
	}
	
	func action1(string:String) {
		NSLog("Action1 %@ from %x",string, self)
	}
	
	
	func doFunctions() {
		
		// Closure Test
		closure = {
			/* this requires weak/unowned */
			[weak self] in
			self?.action1("I'm in closure")
		}
		
		closureOwner.closure = {
			/* this requires weak/unowned because closureOwner is an instance variable*/
			[weak self] in
			self?.doNothing()
		}
		
		let anotherClosureOwnder = ClosureOwner()
		anotherClosureOwnder.closure = {
			/* this does not require weak/unowned */
			self.doNothing()
		}
		
		
		// Array test
		
		// Array is not NSMutableArray
		print("* Adding an object to Array using 'inout'")
		var array1:[String] = []
		let array1ptr = array1
		function1(&array1) // inout is not exact same as passing NSObj
		print(array1)
		if (array1ptr as NSArray) === (array1 as NSArray) {
			print("array1 \(array1.count) === array1ptr \(array1ptr.count)")
		}else {
			print("array1 \(array1.count) !== array1ptr \(array1ptr.count)")
		}
		print("inout is not exact same as passing NSObj\n")

		
		print("* Conventional way adding an object to NSMutableArray")
		let array3 = NSMutableArray()
		let array3ptr = array3
		function3(array3)
		print(array3)
		if array3ptr === array3 {
			print("array3 === array3ptr")
		}else {
			print("array3 !== array3ptr")
		}
		print("array3ptr is still the same as array3")
		
		
		// String is not NSMutableString
		let string1 = ""
		appendString(string1)
		print("This doesn't append String \(string1)")
		
		let mutableString = NSMutableString()
		appendString2(mutableString)
		print("NSMutableString \(mutableString)")
		
		// Dictionary is not NSMutableDictionary
		let dict1:[String:AnyObject] = [:]
		appendDictionary(dict1)
		print("Dictionary \(dict1["Key1"])")
		
		
		// Class is Class
		let dog1 = Dog()
		let dog2 = dog1
		manipulateDog(dog1)
		if dog1 === dog2 {
			print("Still the same")
		}
		print("dog1 \(dog1.breed)")
		print("dog2 \(dog2.breed)")
		
		// Struct
		var dog3 = DogStruct()
		let dog4 = dog3
		dog3.breed = "Snoopy"
		print("dog3 \(dog3.breed)")
		print("dog4 \(dog4.breed)")

		
		// Defer test
		deferTest(true)
		
		
		// Get test
		// getTest1 returns value as 'get'.
		// getTest2 returns initialized value.
		print("getTest1 = \(getTest1)")
		print("getTest2 = \(getTest2)")
		
		print("getTest1 = \(getTest1)")
		print("getTest2 = \(getTest2)")
	}
	
	func doNothing() {
		print("do nothing")
	}
	
	func function1(inout array:[String]) {
		array.append("String")
	}
	
	func function2(var array:[String]) {
		array.append("String")
	}
	
	func function3(array:NSMutableArray) {
		array.addObject("String")
	}
	
	func manipulateDog(dog:Dog) {
		dog.breed = "Snoopy"
	}
	
	func appendString(var string:String) {
		string.appendContentsOf("Extra")
	}
	
	func appendString2(string:NSMutableString) {
		string.appendString("Extra")
	}
	
	func appendDictionary(var dictionary:[String:AnyObject]) {
		dictionary["Key1"] = NSNull()
	}
	
	func deferTest(flag:Bool) {
		
		if flag == false { return } // Defer closure is not executed
		var status = ""
		defer{ print("deferTest ended \(status)") }
		
		
		//Execute later
		let delayInSeconds = 1.0
		let popTime = dispatch_time(
			DISPATCH_TIME_NOW,
			Int64(delayInSeconds * Double(NSEC_PER_SEC))
		)
		
		dispatch_after(popTime, dispatch_get_main_queue(), {
			/* this requires weak/unowned because Closure 'self' and the closure doesn't capture mutually */
			print("dispatch_after finished")
			self.doNothing()
		})
		
		status = "dispatch_after dispatched"
	}
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	
	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		
		let obj = MyObject()
		obj.doFunctions()
		
		return true
	}
}

